# Cornell ASIST LW Testbed API

## Server-side component for Cornell ASIST Lightweight testbed

This code repository is part of a the Cornell ASIST lightweight (LW) tesbed built for the DARPA ASIST project. It provides the [Cornell LW testbed client](https://gitlab.com/cu_asist/testbed/asist_client) with persistence and synchronization of game data, and handles all of the database functions.  

It uses [`Uvicorn`/`FastAPI`](https://www.uvicorn.org/#fastapi) as its web framework, [`REDIS`](https://redis.com/) for short-term persistence and [`Firebase`](https://firebase.google.com/) for long-term storage. Clients connect to the API using websockets.

Please see the [LW testbed client](https://gitlab.com/cu_asist/testbed/asist_client) repository for all detailed documentation. 
